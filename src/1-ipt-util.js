/*
	util js工具函数
*/
!function(exports){
	    function each(arr,fn){
			for(var i=0,l=arr.length,t;i<l;i++){
				t=arr[i];
				if(fn(t,i)===false)break;
			}
		}
		//遍历 并列执行 结果反馈给回调
		function map(arr,fn,call){
			if(Object.prototype.toString.call(arr)==='[object Array]'){
				var rs=[],rerr=null,num=1;
				for(var i=0,l=arr.length;i<l;i++){
					!function(t,i){
						fn(function(err,res){
							if(err)rerr=err;
							else rs[i]=res;
							if(num++==l)call(rerr,rs);
						},t,i);
					}(arr[i],i)
				}
			}else if(Object.prototype.toString.call(arr)==='[object Object]'){
				var keys=[],num=1,rs={},rerr=null;
				for(var key in arr)keys.push(key);
				for(var key in arr){
					!function(t,key){
						fn(function(err,res){
							if(err)rerr=err;
							else rs[key]=res;
							if(num++==keys.length)call(rerr,rs);
						},t,key);
					}(arr[key],key)
				}
			}
		}
		//遍历 序列执行 结果分级向下反馈
		function mapsear(arr,fn,call){
			if(Object.prototype.toString.call(arr)==='[object Array]'){
				var rs=[],rerr=null,len=arr.length;
		        !function run(i){
		            if(i==len){
		                call(null,rs);
		                return;
		            }
		            var a=arr[i];
		            fn(function(err,para){
		                if(err){
		                    call(err);
		                    return;
		                }
		                rs[i]=para;
		                run(i+1);
		            },a,i);
		        }(0);
			
			}else if(Object.prototype.toString.call(arr)==='[object Object]'){
				var keys=[],rs={};
				for(var key in arr)keys.push(key);
		        var len=keys.length;
		        !function run(i){
		            if(i==len){
		               call(null,rs);
		               return;
		            }
		            var a=arr[keys[i]];
		            fn(function(err,para){
		                if(err){
		                    call(err);
		                    return;
		                }
		                rs[keys[i]]=para;
		                run(i+1);
		            },a,keys[i]);       
		        }(0);
			}
		}
		exports.util={
			each:each,
			map:map,
			mapsear:mapsear
		}
    }(this);