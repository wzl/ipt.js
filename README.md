#ipt.js

###异步引入js css

```
<script>
	ipt('./a.js',function(err,node){
		//err 如果出错err为错误信息
		//node 引入a.js的script标签
	});
	/*
	*并行加载a.js 和a.css 
	*/
	ipt('./a.js')('./a.css')
	/*
	*以下代码等同于 上面的代码
	*/
	ipt('./a.js').ipt('./a.css');
	
	
	ipt('./a.js');
	ipt('./a.css')
</script>
```

###wait函数

```
<script>
	/*
	*并行加载a.js,b.js,c.js当三个js加载完成 加载c.js
	*wait()('./c.js') 等同于 wait().ipt('./c.js')
	*/
	ipt('./a.js')('./b.js')('./c.js').wait()('./c.js');
	/*
	*以下代码等同于 上面的代码
	*/
	ipt('./a.js')('./b.js')('./c.js');
	ipt.wait()('./c.js');
	
	/*
	*err 如果出错 为出错信息
	*nodes 对应前面引入的资源
	*/
	ipt('a.js')('b.js').wait(function(err,nodes){
		//nodes->[a.js的script标签,b.js的script标签]
	})
</script>
```