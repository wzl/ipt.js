/*
	util js工具函数
*/
!function(exports){
	    function each(arr,fn){
			for(var i=0,l=arr.length,t;i<l;i++){
				t=arr[i];
				if(fn(t,i)===false)break;
			}
		}
		//遍历 并列执行 结果反馈给回调
		function map(arr,fn,call){
			if(Object.prototype.toString.call(arr)==='[object Array]'){
				var rs=[],rerr=null,num=1;
				for(var i=0,l=arr.length;i<l;i++){
					!function(t,i){
						fn(function(err,res){
							if(err)rerr=err;
							else rs[i]=res;
							if(num++==l)call(rerr,rs);
						},t,i);
					}(arr[i],i)
				}
			}else if(Object.prototype.toString.call(arr)==='[object Object]'){
				var keys=[],num=1,rs={},rerr=null;
				for(var key in arr)keys.push(key);
				for(var key in arr){
					!function(t,key){
						fn(function(err,res){
							if(err)rerr=err;
							else rs[key]=res;
							if(num++==keys.length)call(rerr,rs);
						},t,key);
					}(arr[key],key)
				}
			}
		}
		//遍历 序列执行 结果分级向下反馈
		function mapsear(arr,fn,call){
			if(Object.prototype.toString.call(arr)==='[object Array]'){
				var rs=[],rerr=null,len=arr.length;
		        !function run(i){
		            if(i==len){
		                call(null,rs);
		                return;
		            }
		            var a=arr[i];
		            fn(function(err,para){
		                if(err){
		                    call(err);
		                    return;
		                }
		                rs[i]=para;
		                run(i+1);
		            },a,i);
		        }(0);
			
			}else if(Object.prototype.toString.call(arr)==='[object Object]'){
				var keys=[],rs={};
				for(var key in arr)keys.push(key);
		        var len=keys.length;
		        !function run(i){
		            if(i==len){
		               call(null,rs);
		               return;
		            }
		            var a=arr[keys[i]];
		            fn(function(err,para){
		                if(err){
		                    call(err);
		                    return;
		                }
		                rs[keys[i]]=para;
		                run(i+1);
		            },a,keys[i]);       
		        }(0);
			}
		}
		exports.util={
			each:each,
			map:map,
			mapsear:mapsear
		}
    }(this);
/*=======================*/
/*
 动态引入资源 实现代码
 函数执行顺序:
 ipt->start->loadSrc->loadCss
 					  loadScript
*/
!function(exports,map,mapsear,each){
	    var p={
	    	pres:[],
	    	timeid:null,
	    	status:'done',
	    	loadsrcs:[]
	    };
	    function loadSrc(src,fn){
	    	if(~src.split('?')[0].indexOf('.css')){
	    		loadCss(src,fn);
	    	}else if(~src.split('?')[0].indexOf('.js')){
	    		loadScript(src,fn);
	    	}
	    }
	    function loadCss(src,fn){
	    	var node=document.createElement('link');
	    	node.rel='stylesheet';
	    	node.href=src;
	    	document.head.insertBefore(node,document.head.firstChild);
	    	if(node.attachEvent){
	    		node.attachEvent('onload', function(){fn(null,node)});
	    	}else{
			   setTimeout(function() {
		         poll(node, fn);
		       }, 0); // for cache
	    	}
	    	function poll(node,callback){
			    var isLoaded = false;
			    if(/webkit/i.test(navigator.userAgent)) {//webkit
		        	if (node['sheet']) {
		        		isLoaded = true;
		      		}
			    }else if(node['sheet']){// for Firefox
			    	try{
			        	if (node['sheet'].cssRules) {
			          		isLoaded = true;
			        	}
			      	}catch(ex){
			        	// NS_ERROR_DOM_SECURITY_ERR
				        if (ex.code === 1000) {
				         	isLoaded = true;
				        }
				    }
			    }
			    if(isLoaded){
			    	setTimeout(function(){
			    		callback(null,node);
			    	},1);
			    }else{
			    	setTimeout(function(){
			    		poll(node,callback);
			    	},10);
			    }
	    	}
	    	node.onLoad=function(){
	    		fn(null,node);
	    	}
	    }
	    function loadScript(src,fn){
	    	var node = document.createElement("script");
	    	node.setAttribute('async','async');
			var timeID
			var supportLoad = "onload" in node
			var onEvent = supportLoad ? "onload" : "onreadystatechange"
			node[onEvent] = function onLoad() {
			    if (!supportLoad && !timeID && /complete|loaded/.test(node.readyState)) {
			        timeID = setTimeout(onLoad)
			        return
			    }
			    if (supportLoad || timeID) {
			        clearTimeout(timeID)
			        fn(null,node);
			    }
			}
			document.head.insertBefore(node, document.head.firstChild);
			node.src=src;
			node.onerror=function(e){
				fn(e);
			}
	    }
	    function ipt(src,fn){
	    	if(~p.loadsrcs.indexOf(src))return arguments.callee;
	    	p.loadsrcs.push(src);
	    	p.pres.push({src:src,fn:fn});
	    	start();
	    	return arguments.callee;
	    }
	    ipt.wait=function(fn){
	    	p.pres.push({fn:fn,wait:true});
	    	start();
	    	return this;
	    }
	    ipt.ipt=ipt;
	    function start(){
	    	if(p.pres.length==0||p.status=='runing')return;
	    	clearTimeout(p.timeid);
	    	p.timeid=setTimeout(function(){
	    		p.status="runing";
	    		var a2rr=[],i=0,waitfn=[function(){}];
	    		each(p.pres,function(t,i1){
	    			if(t.wait){
	    				waitfn[i]=t.fn;
	    				i++;
	    				return;
	    			}
	    			a2rr[i]=a2rr[i]||[];
	    			a2rr[i].push(t);
	    		});
	    		p.pres=[];
	    		mapsear(a2rr,function(end,t,i){
	    			map(t,function(end1,t1,i1){
	    				loadSrc(t1.src,function(err,node){
	    					if(t1.fn)t1.fn(err,node);
	    					end1(err,node);
	    				});
	    			},function(err,rs){
	    				if(waitfn[i])waitfn[i](err,rs);
	    				end(err,rs);
	    			});
	    		},function(err,rs){
	    			if(err){
						console.log('diqye-err',err);
						return;
					}
					p.status='done';
	    			start();
	    		});
	    		
	    	});
	    }
	    exports.ipt=ipt;
    }(this,util.map,util.mapsear,util.each);